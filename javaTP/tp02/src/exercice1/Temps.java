package exercice1;

public class Temps {
	
	private int heure; 
	private int minute; 
	private int seconde; 
	
	public Temps(int heure, int minute, int seconde){
		
		setHeure(heure);
		setMinute(minute);
		setSeconde(seconde);
	}
	
	public Temps(int seconde){
		setHeure((int)seconde/3600);
		setMinute((int)((seconde-(heure*3600))/60));
		setSeconde((int)(seconde-(heure*3600 + minute*60)));
	}
	
	public int valeurEnSeconde()
	{
		int secondesF = getSeconde();
		secondesF += getMinute()*60;
		secondesF += getHeure()*60*60;
		return secondesF;
	}
	
	public int getHeure() {
		return heure;
	}

	public void setHeure(int heure) {
		this.heure = heure;
	}

	public int getMinute() {
		return minute;
	}

	public boolean setMinute(int minute) {
		if(minute >= 0 && minute < 60)
		{
			this.minute = minute;
			return true;
		}
		else
		{
			System.err.println("ERROR : MINUTE "+minute+" IS NOT CORRECT");
			return false;
		}
	}

	public int getSeconde() {
		return seconde;
	}

	public boolean setSeconde(int seconde) {
		if(seconde >= 0 && seconde < 60)
		{
			this.seconde = seconde;
			return true;
		}
		else
		{
			System.err.println("ERROR : SECONDE "+seconde+" IS NOT CORRECT");
			return false;
		}
	}

	public void Afficher()
	{
		System.out.println(getHeure()+":"+getMinute()+":"+getSeconde());
	}
}
