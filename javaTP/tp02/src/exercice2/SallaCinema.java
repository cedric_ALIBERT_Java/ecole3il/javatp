package exercice2;

public class SallaCinema {
	final private String nomDuFilm;
	final private int nbPlace;
	final private double tarif;
	private int nbPlaceNormale = 0;
	private int nbPlaceReduite = 0;

	public SallaCinema(String nomDuFilm, int nbPlace, double tarif)
	{
		this.nomDuFilm = nomDuFilm;
		this.nbPlace = nbPlace;
		this.tarif = tarif;;
	}

	public String getNomDuFilm() {
		return nomDuFilm;
	}


	public int nombrePlacesDisponibles() {
		return nbPlace;
	}
	
	public void vendrePlaces(int nombre, boolean tarifReduit)
	{
		if (tarifReduit)
		{
			if(nombre <= (this.nbPlace-(this.nbPlaceNormale+this.nbPlaceReduite)))
			{
				this.nbPlaceReduite += nombre;
				System.out.println("Vous devez payer : " +nombre*this.tarif);
			}
			else 
				System.err.println("Il y a plus assez de place pour votre vente.");
		}
		else
		{
			if(nombre <= (this.nbPlace-(this.nbPlaceNormale+this.nbPlaceReduite)))
			{
				this.nbPlaceNormale += nombre;
				System.out.println("Vous devez payer : " +nombre*this.tarif*0.2); // tarif reduit 80 %
			}
			else 
				System.err.println("Il y a plus assez de place pour votre vente.");
		}
			
	}
	
	public void remiseAZero()
	{
		this.nbPlaceReduite = 0;
		this.nbPlaceNormale = 0;
	}
	
	public double chiffreAffaires()
	{
		return this.nbPlaceNormale*this.tarif+this.nbPlaceReduite*this.tarif*0.8;
	}
	public double tauxRemplissage()
	{
		return 100*(this.nbPlaceNormale+this.nbPlaceReduite)/this.nbPlace;
	}
	
	public void afficherInfos()
	{
		System.out.println("");
		System.out.println("===============");
		System.out.println("Film Projet� : " +this.nomDuFilm);
		System.out.println("Nombre de places : " +this.nbPlace);
		System.out.println("Prix d'une place : " +this.tarif);
		System.out.println("Places vendues au tarif normal : " +this.nbPlaceNormale);
		System.out.println("Places vendues au tarif reduit : " +this.nbPlaceReduite);
		System.out.println("Places disponibles : " + (this.nbPlace-(this.nbPlaceNormale+this.nbPlaceReduite)) );
		System.out.println("Taux remplissage : " +((this.nbPlaceNormale+this.nbPlaceReduite)*100/this.nbPlace));
		System.out.println("Chiffre d'affaire : " +(this.nbPlaceNormale*this.tarif+this.nbPlaceReduite*this.tarif*0.8));
		System.out.println("");
	}

	public double getTarif() {
		return tarif;
	}


	public int getNbPlaceNormale() {
		return nbPlaceNormale;
	}

	public void setNbPlaceNormale(int nbPlaceNormale) {
		this.nbPlaceNormale = nbPlaceNormale;
	}

	public int getNbPlaceReduite() {
		return nbPlaceReduite;
	}

	public void setNbPlaceReduite(int nbPlaceReduite) {
		this.nbPlaceReduite = nbPlaceReduite;
	}

	// SET and GET 
	
	
	
	
}
