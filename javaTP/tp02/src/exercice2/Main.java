package exercice2;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
			double chiffre = 0.0;
		
			SallaCinema salle1 = new SallaCinema("The Artist",50,7.5);
			SallaCinema salle2 = new SallaCinema("Intouchable",100,8.5);
			
			for (int i = 0; i < 15; i++) {
				salle1.vendrePlaces(4, false);
				
			}
			for (int i = 0; i < 4; i++) {
				salle2.vendrePlaces(7, true);
				salle2.vendrePlaces(4, false);
			}
			
			chiffre += salle1.chiffreAffaires();
			chiffre += salle2.chiffreAffaires();
			
			salle1.afficherInfos();
			salle2.afficherInfos();
			
			salle1.remiseAZero();
			salle2.remiseAZero();
			
			System.out.println("== SEANCE 2 ==");
			
				for (int i = 0; i < 3; i++) {
					salle1.vendrePlaces(4, false);
				}
			
				salle2.vendrePlaces(7, true);
				salle2.vendrePlaces(7, true);
				
				salle2.vendrePlaces(4, false);
				salle2.vendrePlaces(4, false);
				
				chiffre += salle1.chiffreAffaires();
				chiffre += salle2.chiffreAffaires();
				
				salle1.afficherInfos();
				salle2.afficherInfos();
				
				System.out.println("Chiffre total de la soir�e : " + chiffre);
				
				
	}

}
