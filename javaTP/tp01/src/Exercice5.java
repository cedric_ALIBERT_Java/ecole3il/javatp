
public class Exercice5 {

	public static int Alea()
	{
		int nombre = 1+ (int)(Math.random()*100);
		return nombre;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] monTableau;
		int taille = 100;
		monTableau = new int[taille];
		int pluspetit = taille;
		int plusgrand = 0;
		double moyenne = 0;
		boolean debut = true;
			for (int current : monTableau) 
			{
				current = Alea();
				if(pluspetit > current)
					pluspetit = current;
				if(plusgrand < current)
					plusgrand = current;
				moyenne = moyenne+current;
				if(debut)
				{
					System.out.print(current );
					debut = false;
				}
				else
					System.out.print("," +current);
			}
			moyenne = moyenne/taille;
			System.out.println(" Moyenne du tableau : "+moyenne+" / Plus grand : "+plusgrand+" / plus petit : "+pluspetit+" / taille du tablau : "+taille);
	}

}
