
import java.util.Scanner;

public class Exercice3 {

	public static int Alea()
	{
		int nombre = 1+ (int)(Math.random()*100);
		return nombre;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int alea = Alea();
		int essais = 0;
		boolean erreur = false;
		System.out.println("Le nombre aleatoire est : " + alea );
		Scanner entre = new Scanner(System.in);
		System.out.println("Vous devez trouver un nombre entre 1 et 100 entrez des propositions, l'ordinateur vous dira si c'est plus ou moins.");
		System.out.println("Proposition :");
		int proposition = -1;
		while(proposition != alea && proposition !=0)
		{
			if(entre.hasNextInt())
			{
				proposition = entre.nextInt();
			}
			else
			{
				erreur = true;
				System.out.print("Erreur de variable : ");
				entre.next();
			}
			if (proposition > 0 && proposition <= 100 && !erreur)
			{
				essais++;
				if(proposition > alea)
				{
					System.out.println("C'est plus petit !");
				}
				else
				{
					System.out.println("C'est plus grand");
				}
			}
			else 
			{
				System.out.println("Vous avez fait un erreur ! le nombre est compris entre 0 et 100");
				erreur = false;
			}
			System.out.print("Recommencez : ");
			
		}
		if(proposition != 0)
		{
			System.out.println("Bravo ! Vous avez trouver en "+essais+" essais! ");
		}
		else
		{
			System.out.println("Vous avez abandonner apr�s "+essais+ " essais. ");
		}
		entre.close();
	}

}
