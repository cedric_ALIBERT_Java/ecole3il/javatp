package test;



public class Livre {

		private String titre ;
		private double prix;

		public Livre(String titre, double prix)
		{
			this.titre = titre;
			this.prix = prix;
		}
		
		public double getPrix() {
			return prix;
		}
		public String getTitre() {
			return titre;
		}
	
		
		
}
