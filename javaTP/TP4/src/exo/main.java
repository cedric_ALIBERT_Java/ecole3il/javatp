package exo;

public class main {

	public static void main(String[] args) {
		
		Personne P1 = new Personne("Laroche","Isabelle",3);
		Personne P2 = new Personne("Dupont","Jacques",1);
		
		Adresse A1 = new Adresse("44 Rue Sainte-Anne","87000","Limoges");
		Adresse A2 = new Adresse("7 Allée des pins","17200","Royan");
		
		System.out.println(P1.toString());
		System.out.println(P2.toString());
		
		System.out.println(A1.toString());
		System.out.println(A2.toString());
		

	}

}
