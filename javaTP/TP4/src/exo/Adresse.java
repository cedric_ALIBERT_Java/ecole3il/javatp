package exo;

public class Adresse {
	private String voie;
	private String cp;
	private String ville;
	
	Adresse(String vo, String c, String vi) {
		setVoie(vo);
		setCp(c);
		setVille(vi);
	}
	
	public String getVoie() {
		return voie;
	}
	public void setVoie(String voie) {
		this.voie = voie;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		boolean tmpTest=false;
		char tmpChar;
		
		if( cp.length() == 5) {
			for(int i=0;i<5;i++) {
				tmpChar = cp.charAt(i);
				if( !Character.isDigit(tmpChar) )
					tmpTest = true;
			}			
			
			if( !tmpTest )
				this.cp = cp;
		}
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville.toUpperCase();
	}
	public String toString() {
		return getVoie() + " / " + getCp() + " " + getVille();
	}
}
