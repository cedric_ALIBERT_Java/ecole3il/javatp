package exo;

public class Personne {

	private String nom;
	private String prenom;
	private int CodeCivilite;
	
	public Personne(String nom, String prenom, int CodeCivilite)
	{
		setNom(nom);
		setPrenom(prenom);
		setCodeCivilite(CodeCivilite);
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom.toUpperCase();
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getCodeCivilite() {
		return CodeCivilite;
	}
	public void setCodeCivilite(int civile) {
		if(1 <= civile && civile <= 3)
			this.CodeCivilite = civile;
	}
	public String getTexteCivilite()
	{
		if(CodeCivilite == 1)
			return "M.";
		else if(CodeCivilite == 2)
			return "Mme";
		else if(CodeCivilite == 3)
			return "Mlle";
		else return "";
	}
	
	public String toString()
	{
		return getTexteCivilite() + " " + getNom() + " " + getPrenom();
	}
	
	
	
	
}
