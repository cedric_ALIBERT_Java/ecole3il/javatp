package exercice1;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String chaine = "Institut d'Ingénierie Informatique de Limoges";
		System.out.println("Longueur : " + chaine.length());
		System.out.println("majusucule : "+ chaine.toUpperCase());
		System.out.println("minuscule : "+ chaine.toLowerCase());
		System.out.println("tiret : "+ chaine.replace(' ','-'));
		System.out.println("sans espace : "+ chaine.replace(" ",""));
		System.out.println("2eme caractere : "+ chaine.charAt(1));
		System.out.println("1 er t : "+ chaine.indexOf('t'));
		System.out.println("dernier t: "+ chaine.lastIndexOf('t'));
		System.out.println("du premier au dernier t  : "+ chaine.substring(chaine.indexOf('t'), chaine.lastIndexOf("t")));
		System.out.println("tiret : "+ chaine.replace(' ','-'));
		
		String chaine2 = chaine.toLowerCase();
		System.out.print("position des i : ");
		int n = 0;
		int rang = 0;
		while (chaine2.indexOf('i',rang) != -1) {
			System.out.print(chaine2.indexOf('i',rang) + " ");
			rang = chaine2.indexOf('i',rang)+1;
			n++;
		}
		System.out.println();
		System.out.print("nombre de i : "+n);
	}

}
