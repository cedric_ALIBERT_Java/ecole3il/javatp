package exercice3;

public class Ecole {

		private String nom; 
		private String nom_usuel;
		private String pays;
		private String ville;
		private String url;
		
		public String toString()
		{
			return nom_usuel+" ( "+nom+" ) - "+ ville + " ( "+pays+" )" + " url : " + url;
		}
		
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			nom = nom.substring(0, (nom.indexOf("(")-1));
			this.nom = nom.replaceAll("&#039;", "'");
		}
		public String getNom_usuel() {
			return nom_usuel;
		}
		public void setNom_usuel(String nom_usuel) {
			
			this.nom_usuel = nom_usuel.substring((nom_usuel.indexOf("(")+1), (nom_usuel.indexOf(")")-1));
		}
		public String getPays() {
			return pays;
		}
		public void setPays(String pays) {
			this.pays = pays.toUpperCase();
		}
		public String getVille() {
			return ville;
		}
		public void setVille(String city) {
			String ville;
			char tmp = city.charAt(0);
			StringBuilder tuyau = new StringBuilder();
			
			city = city.toLowerCase();
			
			for(int i=0; i<city.length(); i++) {
				tuyau.insert(i, city.charAt(i));

				if ( i == 0 || tmp == '-' || tmp == '\'' || tmp == ' ' ) {
					tuyau.setCharAt(i, Character.toUpperCase(city.charAt(i)));
				}
				
				tmp = city.charAt(i);
			}
			
			ville = tuyau.toString();
			ville = ville.replaceAll("&#039;", "'");
			this.ville = ville;
		}
		
		
		static public String foudUrl(String lien) throws Exception
		{
				System.setProperty("http.proxyHost", "172.16.105.254");
				System.setProperty("http.proxyPort", "8082");
				
				java.net.URL url = new java.net.URL("http://extranet.cti-commission.fr"+lien);
				java.io.InputStreamReader isr = new java.io.InputStreamReader( url.openStream());
				java.io.BufferedReader in = new java.io.BufferedReader(isr);
				
				StringBuilder page = new StringBuilder();
				String ligne;
				
				while ( (ligne = in.readLine() ) != null)
				{
					page.append(ligne.trim());
				}
				
				in.close();
				
				//Site : <a href=
				int i = 0;
				i = page.indexOf("Site : <a href=",i);
				return page.substring(i+16,(page.indexOf("target",i)-2));
				
				
				
		}
		
		
		static public void found(String lien) throws Exception
		{
			System.setProperty("http.proxyHost", "172.16.105.254");
			System.setProperty("http.proxyPort", "8082");
			
			java.net.URL url = new java.net.URL(lien);
			java.io.InputStreamReader isr = new java.io.InputStreamReader( url.openStream());
			java.io.BufferedReader in = new java.io.BufferedReader(isr);
			
			StringBuilder page = new StringBuilder();
			String ligne;
			
			while ( (ligne = in.readLine() ) != null)
			{
				page.append(ligne.trim());
			}
			
			in.close();
			
			String code = page.toString();
			code = new String(code.getBytes(),"UTF-8");
			
			int i=0;
			while((code.indexOf("td_name",i) != -1))
			{
				Ecole ecole = new Ecole();
				i = code.indexOf("td_name",i);
				i = i+9;
				ecole.setNom(code.substring(i,code.indexOf("</td>",i)));
				ecole.setNom_usuel(code.substring(i,code.indexOf("</td>",i)));
				i = code.indexOf("td_pays",i);
				i = i+9;
				ecole.setPays(code.substring(i,code.indexOf("</td>",i)));
				i = code.indexOf("td_ville",i);
				i = i+10;
				ecole.setVille(code.substring(i,code.indexOf("</td>",i)));
				
				i = code.indexOf("action_informations",i);
				i = i+30;
				ecole.setUrl( foudUrl(code.substring(i,(code.indexOf(">",i)-1))) ); 
				
				System.out.println(ecole.toString());
			}
		}
		
		
		
}
