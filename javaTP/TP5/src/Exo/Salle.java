package Exo;

public class Salle {
	private final int longueur;
	private final int largeur;
	
	private Telerupteur[] telerupteurs;
	private Luminaire[] luminaires;
	private Bouton[] bouttons;
	private char tableauCaractere[] = {'A','B','C','D','E','F','G'};
	
	private int LuminaireCourant = 0;
	private int BouttonCourant = 0;
	private int nbTelerupteur = 0;
	private int nbBoutton = 0;
	private int nbLuminaire = 0;
	
	final char CASE = '\u2591';
	final char CASE_NOIRE = '\u2588';
	
	Salle(int longueur, int largeur, int nbTelerupteur, int nbLuminaire, int nbBoutton){
		if(longueur > 0) 
			this.longueur = longueur;
		else
			this.longueur = 10;
		if(largeur > 0) 
			this.largeur = largeur;
		else 
			this.largeur = 10;
		
		this.nbTelerupteur = nbTelerupteur;
		telerupteurs = new Telerupteur[this.nbTelerupteur];
		for (int i = 0; i < telerupteurs.length; i++) {
			telerupteurs[i] = new Telerupteur(); 
		}
		
		this.nbBoutton = nbBoutton;
		bouttons = new Bouton[nbBoutton];
		
		this.nbLuminaire = nbLuminaire;
		luminaires = new Luminaire[nbLuminaire];
		
	}

	public int getLongueur() {
		return longueur;
	}

	public int getLargeur() {
		return largeur;
	}
	
	public int findLuminaireNumber(int x, int y) // Renvoi une valeur en fonction de x et y afin de savoir s'il y a un luminaire et s'il est allumé ou pas
	{
		int number = 0;
			for (Luminaire Lumi : luminaires) {
				if(Lumi != null) // car on parcourt aussi les élements nulls ...
				{
					if (Lumi.ObtenirX() == x && Lumi.ObtenirY() == y) {
						if(Lumi.estAllume())
							return 2;
						else
							return 1;
					}
					if (Lumi.ObtenirX() == (x-1) && Lumi.ObtenirY() == y) {
						if(Lumi.estAllume())
							return 2;
						else
							return 1;
					}
					
				}
				
			}
			return number;
	}
	
	public char findBouttonNumber(int x, int y) // meme principe que la fonction précedente, sauf qu'elle renvoi la lettre de l'interrupteur
	{
		char retour = ' ';
			for (Bouton but : bouttons) {
				if(but != null) // car on parcourt aussi les élements nulls ...
				{
					if (but.getX() == x && but.getY() == y) {
							retour = but.getLettre();
					}
				}
			}
			return retour;
	}
	

	public void ajouterLuminaire(int x, int y, int numTelerupteur)
	{
		if( x < 0 || x > longueur || y < 0 || y+1 > largeur || numTelerupteur < 0 || numTelerupteur >= telerupteurs.length )
			return;
		
		if( LuminaireCourant >= luminaires.length )
			return;
		
		luminaires[LuminaireCourant] = new Luminaire(x, y, telerupteurs[numTelerupteur]);
		LuminaireCourant++;
	}
	
	public void ajouterBoutton(int x, int y, int numTelerupteur)
	{
		if( x < 0 || x > longueur || y < 0 || y+1 > largeur || numTelerupteur < 0 || numTelerupteur >= telerupteurs.length )
			return;
		
		if( BouttonCourant >= bouttons.length )
			return;
		
		bouttons[BouttonCourant] = new Bouton(x, y, telerupteurs[numTelerupteur], tableauCaractere[BouttonCourant]);
		BouttonCourant++;
	}
	
	public void affiche() {
		for (int i = 0; i < this.largeur; i++) {
			
			for (int j = 0; j < this.longueur; j++) {
				
				int tmp=-1;
				
				if( findLuminaireNumber(j,i) != 0)
					tmp = findLuminaireNumber(j,i); // on a trouvé un luminaire a cet emplacement
				if(tmp == 2)
						System.out.print(" ");
				else
					if(tmp == 1)
							System.out.print(CASE_NOIRE);
				
				if( findBouttonNumber(j,i) != ' ')
				{
					System.out.print(findBouttonNumber(j,i));
					tmp = 0; // sinon affiche une case grise
				}
				
				if(tmp == -1) // s'il n'y a rien, on affiche qu'une case
					System.out.print(CASE);
			}
				
			System.out.println();
			
		}
		
	}
		public void actionnerBoutton(char c)
		{
			for (Bouton bouton : bouttons) {
				if(bouton != null) // car on parcourt aussi les élements nulls ...
				{
					if(bouton.getLettre() == c)
					{
						bouton.actionner();
					}
				}
			}
		}
		
	
	
	
	
	
}
