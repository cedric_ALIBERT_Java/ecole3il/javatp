package Exo;
import java.util.Scanner; 

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/* PREMIERE IMPLANTATION
		 
		Salle S = new Salle(46,9,2,5,6);
		S.ajouterLuminaire(4, 4, 0);
		S.ajouterLuminaire(16, 4, 0);
		S.ajouterLuminaire(28, 4, 0);
		S.ajouterLuminaire(40, 2, 1);
		S.ajouterLuminaire(40, 6, 1);

		S.ajouterBoutton(0, 0, 0);
		S.ajouterBoutton(0, 8, 0);
		S.ajouterBoutton(14, 8, 0);
		S.ajouterBoutton(31, 8, 0);
		S.ajouterBoutton(43, 8, 1);
		S.ajouterBoutton(43, 0, 1);
		*/
		
		Salle S = new Salle(46,9,3,5,6);
		S.ajouterLuminaire(4, 4, 0);
		S.ajouterLuminaire(16, 4, 1);
		S.ajouterLuminaire(28, 4, 1);
		S.ajouterLuminaire(40, 2, 2);
		S.ajouterLuminaire(40, 6, 2);
		
		S.ajouterBoutton(0, 0, 0);
		S.ajouterBoutton(0, 8, 0);
		S.ajouterBoutton(14, 8, 1);
		S.ajouterBoutton(31, 8, 1);
		S.ajouterBoutton(43, 8, 2);
		S.ajouterBoutton(43, 0, 1);
		
		S.affiche();
		String str;
		Scanner sc = new Scanner(System.in);
		
		do
		{
			
			System.out.println("Quel bouton actionner ? :");
			str = sc.nextLine();
			
			char c = (str.toUpperCase()).charAt(0);
			
			S.actionnerBoutton(c);
			
			System.out.println();
			S.affiche();
			
		}while(!str.equals('0'));
	}

}
