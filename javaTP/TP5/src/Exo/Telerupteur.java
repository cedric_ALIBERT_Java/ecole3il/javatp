package Exo;

public class Telerupteur {
	
	private boolean allume = false;
	
	public void basculerEtat()
	{
		allume = !allume;
	}
	
	public boolean estAllume()
	{
		return allume;
	}

}
