package Exo;

public class Luminaire {

	final private int x;
	final private int y;
	final private Telerupteur telerupteur;
	
	Luminaire(int x, int y, Telerupteur telerupteur)
	{
		this.x = x;
		this.y = y;
		this.telerupteur = telerupteur;
	}

	public int ObtenirX() {
		return x;
	}

	public int ObtenirY() {
		return y;
	}
	
	public boolean estAllume(){
		return this.telerupteur.estAllume();
	}
	
	
}
