package Exo;

public class Bouton {
	
	private final int x, y ;
	Telerupteur telerupteur;
	private final char lettre;
	

	Bouton(int x, int y, Telerupteur telerupteur,char lettre)
	{

			this.x = x;
			this.y = y;
			this.telerupteur = telerupteur;
			this.lettre = lettre;
		
	}
	
	
	
	public int getX() {
		return x;
	}



	public int getY() {
		return y;
	}



	public char getLettre() {
		return lettre;
	}



	public void actionner()
	{
		telerupteur.basculerEtat();
	}
	
}
