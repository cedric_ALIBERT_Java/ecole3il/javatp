package exo;

public class CarteDeVisite {
	private Personne personne;
	private Adresse adresse;
	
	CarteDeVisite(Personne p, Adresse a) {
		personne = p;
		adresse = a;
	}
	public Personne getPersonne() {
		return personne;
	}
	public Adresse getAdresse() {
		return adresse;
	}
	public void afficher() {
		System.out.println(personne.toString());
		System.out.println(adresse.toString());
	}
	
}